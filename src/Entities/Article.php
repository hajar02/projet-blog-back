<?php

namespace App\Entities;
use DateTime;

class Article
{
    private ?int $id;
    private ?string $title;
    private ?string $description;
    private ?DateTime $date;
    private ?string $image;
    private ?string $creator;
    private ?int $idCategory;


    /**
     * @param int|null $id
     * @param string|null $title
     * @param string|null $description
     * @param DateTime|null $date
     * @param string|null $image
     * @param string|null $creator
     * @param int|null $idCategory
     */
    public function __construct(?string $title, ?string $description, ?DateTime $date, ?string $image, ?string $creator, ?int $idCategory, ?int $id=null) {
    	$this->id = $id;
    	$this->title = $title;
    	$this->description = $description;
    	$this->date = $date;
    	$this->image = $image;
    	$this->creator = $creator;
    	$this->idCategory = $idCategory;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param string|null $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string {
		return $this->description;
	}
	
	/**
	 * @param string|null $description 
	 * @return self
	 */
	public function setDescription(?string $description): self {
		$this->description = $description;
		return $this;
	}

	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getImage(): ?string {
		return $this->image;
	}
	
	/**
	 * @param string|null $image 
	 * @return self
	 */
	public function setImage(?string $image): self {
		$this->image = $image;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCreator(): ?string {
		return $this->creator;
	}
	
	/**
	 * @param string|null $creator 
	 * @return self
	 */
	public function setCreator(?string $creator): self {
		$this->creator = $creator;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getIdCategory(): ?int {
		return $this->idCategory;
	}
	
	/**
	 * @param int|null $idCategory 
	 * @return self
	 */
	public function setIdCategory(?int $idCategory): self {
		$this->idCategory = $idCategory;
		return $this;
	}
}