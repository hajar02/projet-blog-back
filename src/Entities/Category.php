<?php

namespace App\Entities;

class Category
{
    private ?int $id;
    private ?string $categoryName;

    /**
     * @param int|null $id
     * @param string|null $categoryName
     */
    public function __construct(?string $categoryName, ?int $id=null) {
    	$this->id = $id;
    	$this->categoryName = $categoryName;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCategoryName(): ?string {
		return $this->categoryName;
	}
	
	/**
	 * @param string|null $categoryName 
	 * @return self
	 */
	public function setCategoryName(?string $categoryName): self {
		$this->categoryName = $categoryName;
		return $this;
	}
}