<?php

namespace App\Controller;

use App\Entities\Category;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/category')]
class CategoryController extends AbstractController
{
    private CategoryRepository $repo;

    public function __construct(CategoryRepository $repo)
    {
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all()
    {

        $categories = $this->repo->findAll();
        return $this->json($categories);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $categories = $this->repo->findById($id);
        if (!$categories) {
            throw new NotFoundHttpException();

        }
        return $this->json($categories);

    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        
        $category = $serializer->deserialize($request->getContent(), Category::class, 'json');
        $this->repo->persist($category);

        return $this->json($category, Response::HTTP_CREATED);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $category = $this->repo->findById($id);
        if(!$category){
            throw new NotFoundHttpException();
        }

        $toUpdate = $serializer->deserialize($request->getContent(), Category::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
        
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $category = $this->repo->findById($id);
        if(!$category){
            throw new NotFoundHttpException();
        }

        $this->repo->delete($category);

        return $this->json(null, Response::HTTP_NO_CONTENT);
        
    }

}