<?php

namespace App\Repository;

use App\Entities\Article;
use PDO;
use DateTime;

class ArticleRepository
{
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * @return Article[]
     */
    public function findAll(): array
    {
        /** @var Article[] */
        $articles = [];


        $statement = $this->connection->prepare('SELECT * FROM article');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $articles[] = $this->sqlToArticle($item);
        }
        return $articles;
    }

    /**
     * @param Article $article
     * @return void
     */
    public function persist(Article $article) {
        $statement = $this->connection->prepare('INSERT INTO article (title, description, date, image, creator, idCategory) VALUES (:title, :description, :date, :image, :creator, :idCategory)');
        $statement->bindValue('title', $article->getTitle());
        $statement->bindValue('description', $article->getDescription());
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue('image', $article->getImage());
        $statement->bindValue('creator', $article->getCreator());
        $statement->bindValue('idCategory', $article->getIdCategory());


        $statement->execute();

        $article->setId($this->connection->lastInsertId());

    }

    /**
     * @param int $id 
     * @return Article|null
     */
    public function findById(int $id):?Article {
        $statement = $this->connection->prepare('SELECT * FROM article WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToArticle($result);
        }
        return null;
    }

    /**
     * @param array $line
     * @return Article
     */
    private function sqlToArticle(array $line):Article {
        $date = null;
        if(isset($line['date'])){
            $date = new DateTime($line['date']);
        }
        return new Article($line['title'], $line['description'], $date, $line['image'], $line['creator'], $line['idCategory'], $line['id']);
    }


    public function update(Article $article):void {

        $statement = $this->connection->prepare("UPDATE article SET title=:title, description=:description, date=:date, image=:image, creator=:creator, idCategory=:idCategory WHERE id=:id ");

        $statement->bindValue('title', $article->getTitle());
        $statement->bindValue('description', $article->getDescription());
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue('image', $article->getImage());
        $statement->bindValue('creator', $article->getCreator());
        $statement->bindValue('idCategory', $article->getIdCategory());
        $statement->bindValue('id', $article->getId());

        $statement->execute();

        $article->setId($this->connection->lastInsertId());

     }

     public function delete(Article $article) {
        $statement = $this->connection->prepare('DELETE FROM article WHERE id=:id');
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);
        $statement->execute();

    }

}
