<?php

namespace App\Repository;

use App\Entities\Category;
use PDO;

class CategoryRepository
{
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * @return Category[]
     */
    public function findAll(): array
    {
        /** @var Category[] */
        $categories = [];


        $statement = $this->connection->prepare('SELECT * FROM category');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $categories[] = $this->sqlToCategory($item);
        }
        return $categories;
    }

    /**
     * @param Category $category
     * @return void
     */
    public function persist(Category $category) {
        $statement = $this->connection->prepare('INSERT INTO category (categoryName) VALUES (:categoryName)');
        $statement->bindValue('categoryName', $category->getCategoryName());

        $statement->execute();

        $category->setId($this->connection->lastInsertId());

    }

    /**
     * @param int $id 
     * @return Category|null
     */
    public function findById(int $id):?Category {
        $statement = $this->connection->prepare('SELECT * FROM category WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToCategory($result);
        }
        return null;
    }

     /**
     * @param array $line
     * @return Category
     */
    private function sqlToCategory(array $line):Category {
       
        return new Category($line['categoryName'], $line['id']);
    }


    public function update(Category $category):void {

        $statement = $this->connection->prepare("UPDATE category SET categoryName=:categoryName WHERE id=:id ");

        $statement->bindValue('categoryName', $category->getcategoryName(), PDO::PARAM_STR);
        $statement->bindValue('id', $category->getId(), PDO::PARAM_INT);
        $statement->execute();

        $category->setId($this->connection->lastInsertId());

     }

	 public function delete($id): void
	 {
		 $statement = $this->connection->prepare("DELETE FROM category WHERE id=:id");
		 $statement->bindValue(":id", $id, PDO::PARAM_INT);
		 $statement->execute();
	 }

     public function articlesByCategory(int $idCategory){
        $articles = [];
        $statement = $this->connection->prepare("SELECT category.categoryName, article.title, article.description,
        article.date, article.image, article.creator
        FROM category
        INNER JOIN article ON category.id=article.idCategory
        WHERE idCategory=:idCategory");

        $statement->bindValue('idCategory', $idCategory);
        $statement->execute();
        $result = $statement->fetchAll();
        if ($result){
            foreach($result as $item){
                $articles[] = $item;
            }
            return $articles;
            }
        return null;
        }

}
