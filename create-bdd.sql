-- Active: 1673947797623@@127.0.0.1@3306@blog

DROP DATABASE blog;

CREATE DATABASE blog;

USE blog;


CREATE TABLE category (
    id INT PRIMARY KEY AUTO_INCREMENT,
    categoryName VARCHAR(30) NOT NULL
);
INSERT INTO category (categoryName) VALUES ('places'), ('cuisine'), ('traditions'), ('history');

CREATE TABLE article (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(50) NOT NULL,
    description VARCHAR(700) NOT NULL,
    date DATE NOT NULL,
    image VARCHAR(100) NOT NULL,
    creator VARCHAR(40) NOT NULL,
    idCategory INT NOT NULL,
    FOREIGN KEY (idCategory) REFERENCES category(id) ON DELETE CASCADE
);

INSERT INTO article (title, description, date, image, creator, idCategory) VALUES 
('Cuscus', 'descrizione per cuscus', '2023-02-09', 'https://i.pinimg.com/236x/ea/c7/69/eac769de2b3cd0c53197a3adf762c498.jpg', 'Hajar', '2'),
('Tajine', 'descrizione per tajine', '2023-02-09', 'https://i.pinimg.com/236x/54/6a/dc/546adc0085c8bd138618bde31b7d42c2.jpg', 'Sara', '2'),
('Marrakech', 'descrizione per marrakech', '2023-02-09', 'https://i.pinimg.com/236x/23/a4/e7/23a4e7d76098a8850e6ee93f9146ea04.jpg', 'Sara', '1'), 
('Mariage', 'Descrizione per mariage', '2023-02-09', 'https://i.pinimg.com/236x/24/19/4d/24194da179f692bb26cc64f7536a264a.jpg', 'Imad', '3'), 
('colonialism', 'Descrizione colonialismo', '2023-02-08', 'https://i.pinimg.com/236x/68/35/2e/68352ec2ed6c882f403217a436180d3e.jpg', 'Nourhen', '4'), 
('Guerre du rif', 'Descrizione per guerra del rif', '2023-02-07', 'https://i.pinimg.com/236x/04/28/4e/04284e57377f7515722189235dbf672e.jpg', 'Said', '4');



