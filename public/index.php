<?php

use App\Kernel;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

return function (array $context) {
    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};

/*require '../vendor/autoload.php';

use App\Entities\Article;
use App\Entities\Category;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;



/* ARTICLE

    // CREATE
        $instance = new ArticleRepository();
        $article = new Article('nuovo', 'descrizione', new DateTime('2000-02-20'), 'https://i.pinimg.com/236x/b7/62/78/b76278017e1b943cf3a82c4ab32fdac3.jpg', 'hajar', '3');
        $instance->persist($article);
    // READ
        var_dump($instance->findAll());
    // UPDATE
        $updateArticle = new Article('milano', 'des', new DateTime('2000-02-23'), 'immagine', 'Said', '1', '1');
        $instance->update($updateArticle);
    // DELETE
        $instance = new ArticleRepository();
        $instance->delete(1);
*/



/* CATEGORY
    // CREATE
        $instance = new CategoryRepository();
        $category = new Category('cinema');
        $instance->persist($category);
    // READ
        var_dump($instance->findAll());
    // UPDATE
        $updateCategory = new Category('football', '2');
        $instance->update($updateCategory);
    // DELETE
        $instance = new CategoryRepository();
        $instance->delete(1); 
    */    


